Manual en Español
===========================




## Registro e inicio de sesión
Si no tienes una cuenta en TwinPush, puedes registrarte en [app.twinpush.com](https://app.twinpush.com/). Los clientes Enterprise, disponen de un subdominio personalizado para el acceso: [tuempresa.twinpush.com](mailto:info@twincoders.com), contacta con nosotros para obtener tu propio sub-dominio.

Desde esta pantalla podrás:

1. **Registrarte como nuevo usuario** en: Crear una cuenta de TwinPush
2. **Recuperar tu contraseña** en: ¿Has olvidado la contraseña?
3. **Iniciar sesión en TwinPush** con tu usuario y contraseña, si ya te has registrado previamente.

[![](http://i.imgur.com/bie80EEl.png)](http://i.imgur.com/bie80EE.png)


### Registro
Escribe tu correo electrónico y contraseña. Recuerda que al registrarte estas aceptando las [condiciones de uso](http://app.twinpush.com/res/terms-of-service-es.pdf).

[![](http://i.imgur.com/vVTilc2l.png)](http://i.imgur.com/vVTilc2.png)



### Recuperación de contraseña
Escribe la dirección de correo que usaste para registrarte y en breve tiempo recibirás un correo con instrucciones para restaurar tu contraseña.

[![](http://i.imgur.com/wC1Ahfvl.png)](http://i.imgur.com/wC1Ahfv.png)




## Apps
La primera vez que accedes, te pediremos que crees tu primera App, por el momento sólo será necesario el Nombre de tu App y un ícono para tu propia referencia. Más adelante podrás modificar estos parámetros. Al hacer clic en Crear App, estarás creando el espacio de trabajo para tu App.

* **Nombre para tu App**: al crear un nuevo nombre para tu app, intenta describirla tanto como te sea posible, una buena descripción ayudará a que sea facilmente identificable para ti y para tus compañeros. Ejemplo: Clientes Premium Europa Android Production.
* **Icono**: No necesariamente tiene que ser el mismo icono que usarás en el Market sirve para que puedas identificar rápidamente tu app.

[![](http://i.imgur.com/AIrOoCVl.png)](http://i.imgur.com/AIrOoCV.png)



### Tu primera App
El espacio de trabajo para tu App en TwinPush ya está creado y en la pantalla podrás ver el Dashboard o la Vista general de tu App. A continuación, deberás integrar el SDK de TwinPush en tus aplicaciones móviles y subir tu certificado para empezar a enviar notificaciones. Sigue las instrucciones del [SDK aquí](http://www.twinpush.com/quickstart). 

[![](http://i.imgur.com/F6UmoPXl.png)](http://i.imgur.com/F6UmoPX.png)




### Tus siguientes Apps
Una vez creada tu primera App, podrás añadir más Apps en cualquier momento. Si sólo tienes una App, deberás clicar en sobre el ícono o el nombre de tu primera App que se ubica en la barra superior y se desplegará un menú, elige la opción Todas las aplicaciones, a continuación obtendrás una pantalla con todas tus aplicaciones. Clicando sobre Nueva App podrás añadir más Apps. Si ya tienes más de una App, llegarás a esta pantalla al iniciar sesión. Clica sobre una App para entrar en la Vista general de la misma.

[![](http://i.imgur.com/4Tzp7v0l.png)](http://i.imgur.com/4Tzp7v0.png)




## Notificaciones
###Tu Primera Notificación
Para enviar notificiaciones, deberás acceder al Dashboard o Vista general de tu aplicación, clica sobre Push, en la barra lateral izquierda. Si es la primera vez que envías una notificación con esa aplicación verás una pantalla como la siguiente. Clica en Crear la primera o también sobre Crear nueva notificación arriba a la derecha. 

[![](http://i.imgur.com/snBVIRMl.png)](http://i.imgur.com/snBVIRM.png)



Si ya has enviado notificaciones con esa aplicación el botón Push te llevará a la siguiente pantalla. Desde aquí podras ver estadísticas de tus notificaciones enviadas, tus grupos de notificaciones, tus últimas notificaciones, tus campañas, etc. También podras reutilizar grupos, notificaciones o campañas, más adelante te explicaremos cómo. Para crear una nueva notificación desde esta pantalla, clica sobre Crear nueva notificación arriba a la derecha.

[![](http://i.imgur.com/drY3BbKl.png)](http://i.imgur.com/drY3BbK.png)



### Editor de Notificaciones
Al clicar sobre Crear nueva notificación entrarás en el Editor de notificaciones.


El editor de notificaciones se presenta como una serie de pestañas: Push, Content, Target, Schedule, Advanced y Summary, llevándote por defecto a la primera pestaña, Push. También cuenta con una simulación, en la parte derecha de la pantalla, donde se muestra cómo se verá tu notificación al recibirla desde un dispositivo Android o iOS. 

Para enviar una notificación cualquiera, los campos mínimos que hay que completar son: la Alerta, bajo la pestaña Push, y al menos un Target, el resto de opciones del editor te ayudarán a enriquecer tus notificaciones, a realizar acciones cuando el usuario pulse sobre la notificación, a segmentar tus envíos y a programar la hora y el día en que quieres que se envíen tus notificaciones.


#### Push 
Utiliza la pestaña Push para componer tu notificación. La notificación en sí se envía en texto plano. Al componer el mensaje ten en cuenta que los smartphones con sistemas operativos no actualizados estan limitados a un tamaño de notificación de alrededor de 250 caracteres, en estos 250 caracteres están incluidos el texto plano de la notificación y el texto enriquecido o la URL, que se introducen en la pestaña *Content*.

Si quieres que tus notificaciones llegue correctamente a la mayoría de dispositivos, crea mensajes concisos y utiliza URL's cortas cuando puedas.

* **Título**: Es la primera línea de la notificación que aparecerá resaltada en los dispositivos.
* **Alerta**: Escribe aquí el cuerpo de tu mensaje en texto plano. 


[![](http://i.imgur.com/nPC7jRsl.png)](http://i.imgur.com/nPC7jRs.png)



#### Content
En Content podrás seleccionar qué acción adicional realizará la notificación cuando el usuario la abra, de esta manera, podrás enviar a tus usuarios una pantalla con texto enriquecido o una URL externa, adicional a la notificación. Esta función es muy útil en campañas en dónde la notificación lleva un mensaje que despierta el interés del usuario y que puede derivar en acciones inmediatas.

* **None**: Selecciona esta opción para que al abrir la notificación resulte en la apertura de tu App, de manera similar que si lo hiciera desde el ícono de tu App.
* **URL**: introduce una URL con una landing page que se abrirá en el smartphone al momento de abrir la notificación enviada.
* **HTML**: Define un mensaje en texto rico HTML, que podrá contener enlaces externos a URL's o imagenes que se presentará al usuario al momento de abrir la notificación.

[![](http://i.imgur.com/MMk1LJRl.png)](http://i.imgur.com/MMk1LJR.png)



#### Target
Utiliza la pestaña Target para seleccionar qué usuarios quieres que reciban esta notificación. Podrás enviar la notificación a todos los usuarios de tu App, a uno o más usuarios o dispositivos seleccionados manualmente, a tus segmentos, o también podrás importar un fichero que contenga una lista de usuarios o dispositivos.

* **All**: Seleccionando All, la notificación se enviará a todos los usuarios registrados en la plataforma y activos en el momento del envío.
* **User**: Aquí podrás elegir uno o más usuarios o dispositivos escribiéndolos manualmente uno a uno, para ello, identifica a tus usuarios por su Alias, o a los dispositivos por su Device ID. Recuerda que un usuario tiene un Alias, pero puede tener uno o más dispositivos. 
* **Segment**: Elige uno o más segmentos que previamente hayas creado en la sección Segmentos, escribe el nombre del segmento uno a uno en el cuadro de texto.
* **File**: Importa un fichero CSV (.cvs) o de texto plano (.txt) con la lista de usuarios o dispositivos. Podrás personalizar el mensaje que enviarás a cada usuario o dispositivo. [Aquí](https://twincoders.atlassian.net/wiki/x/AgC7AQ) encontrarás las reglas para la creación del fichero.

[![](http://i.imgur.com/4wnAXp7l.png)](http://i.imgur.com/4wnAXp7.png)




#### Schedule
En Schedule eliges cuándo enviar tu notificación, también programar tu envío para que se realice a una hora o día determinados. Twinpush también pone a disposición una potente herramienta para programar campañas de marketing en Crear campaña. En una campaña podrás enviar una notificación, la que podrás repetir el número de veces que indiques, dentro de un periodo establecido. El periodo lo estableces tu mismo seleccionando la fecha de inicio y fin, o el número de días, el rango de horas, y los días de la semana. 

* **Ahora**: La notificación se enviará inmediatamente despues de clicar Enviar que encontrarás al pulsar Guardar y previsualizar.
* **Elegir fecha**: La notificación se enviará en el día y hora establecidos.
* **Crear campaña**: Se enviará la misma notificación repetida un número de veces dentro del periodo establecido, pudiendo establecer los límites de envíos a un mismo dispositivo en un periodo de tiempo.

[![](http://i.imgur.com/sV8KXkPl.png)](http://i.imgur.com/sV8KXkP.png)




#### Advanced
En Advanced, podemos establecer la velocidad de envío de la notificación y también interactuar con la aplicación residente en el smartphone.
Podrás asociar esta notificación a un determinado grupo, de esta manera podrás agrupar tus notificaciones por campañas, periodos, productos, clientes objetivo o lo que tu quieras.

La velocidad de envío es útil cuando se quiere enviar una notificación que conlleve a una acción en enlaces externos a un número elevado de usuarios. Es posible que el pico de tráfico generado por ese gran número de usuarios en ese instante repercuta en la disponibilidad de los servicios de los enlaces externos, perjudicando la experiencia del usuario. TwinPush nos permite distribuir en el tiempo el envío de una misma notificación a un gran número de usuarios. 

Podremos interactuar con la App residente en el Smartphone del usuario, pudiendo alterar el número de Badge de la App, (el badge es el número que aparece en el ícono de la App), el tono musical que sonará al recibir la notificación o la etiqueta que identifica en la App el tipo de notificación recibida, pudiendo configurar en la Aplicaciones acciones concretas según el Tag recibido.

* **Group name**: Añade esta notificación a un grupo existente o a un nuevo grupo, escribe el nombre del grupo aquí.
* **Delivery speed**: Elige la velocidad en que se distribuirá la notificación a tus usuarios.
* **Badge**: Escribe el badge que aparecerá en el Smartphone, por defecto +1 al número que ya tenga el ícono.
* **Sound**: Elige el tono musical que sonará al recibir la notificación, en el caso que tu App disponga de varios sonidos.
* **Tags**: Envía al Smartphone el tipo de notificación que estas enviando, en el caso que tu App disponga de diferentes tipos.

[![](http://i.imgur.com/G2fTJ57l.png)](http://i.imgur.com/G2fTJ57.png)


###Guardar y previsualizar

Una vez hayas completado tu notificación, podrás salvarla y enviarla clicando sobre Guardar y previsualizar abajo a la derecha. TwinPush guardará tu notificación configurada y te mostrará una pantalla resumen. Comprueba que tu notificación este correcta y envíala con el boton Send. Si no lo está, podrás editarla con Edit, o borrarla con Delete. El botón Duplicate te será de gran utilidad para reutilizar notificaciones o para crear tus propias plantillas sobre las cuales puedas hacer variaciones sobre un duplicado de una plantilla original.

[![](http://i.imgur.com/HqKAiuXl.png)](http://i.imgur.com/HqKAiuX.png)



## Segmentos
TwinPush incluye una potente herramienta para segmentar a los usuarios de tu App. Con esta herramienta podrás crear segmentos de usuarios según su ubicación, según las características de su dispositivo como marca, modelo o versión de su sistema, según el perfil de uso de tu aplicación o la información del usuario como su idioma u otra información que tu aplicación haya obtenido del usuario y la haya registrado en la plataforma a través de los "Custom Properties", como por ejemplo: edad, género, ocupación, intereses o cualquier otra información. 



### Tu Primer Segmento
Para crear segmentos, accede al Dashboard o Vista general de tu aplicación y clica sobre Segmentos en la barra lateral izquierda. Si es la primera vez que crear un segmento en esa aplicación, verás una pantalla como la siguiente. Clica en Crear nuevo segmento arriba a la derecha.

[![](http://i.imgur.com/BJ6gPtal.png)](http://i.imgur.com/BJ6gPta.png)



Si ya tienes segmentos creados en tu aplicación verás la siguiente pantalla. Desde aquí obtendrás una rápida vista de todos tus segmentos, donde se muestra para cada uno de ellos, el número de dispositivos, el porcentaje de dispositivos objetivo del segmento (barra verde) sobre el universo de dispositivos totales (barra gris de fondo) y el tipo de filtrado del segmento: por posición, por propiedades del dispositivo o por estadísticas de uso. Para crear un nuevo segmento desde esta pantalla, clica sobre Crear nuevo segmento arriba a la derecha.

[![](http://i.imgur.com/YsiFcvel.png)](http://i.imgur.com/YsiFcve.png)




### Editor de Segmentos
Al clicar sobre Crear nuevo segmento entrarás en el Editor de Segmentos. El editor te llevará por defecto al filtro por Ubicación. El editor de segmentos se presenta como una lista de filtros sobre el margen izquierdo de la pantalla, que podrás ir añadiendo al segmento creado. Los filtros son de tres tipos: por Ubicación, por Datos de Usuario o por Datos del Dispositivo. En el centro de la pantalla encontrarás las opciones de configuración del filtro seleccionado. Sobre la derecha de la pantalla podrás añadir una condición a todos los filtros seleccionados y dar un nombre al segmento recién creado. 
Cada filtro que vas añadiendo al segmento se verá representado a través de un pequeño índice en su correspondiente sección. Este índice indica la cantidad total de filtros totales de esa sección. Así por ejemplo, si añades 2 idiomas distintos, el sub índice en la sección de idiomas será 2, y si añades 3 ciudades, el sub índice en ciudades será 3.

#### Filtros por Ubicación
TwinPush ofrece diferentes maneras de filtrar un grupo de usuarios de acuerdo a su ubicación: puedes crear uno o más geocercos, especificar uno o más países, estados, regiones, o ciudades, para ello clica sobre Coordenadas, País, Estado, Región o Ciudad.  

[![](http://i.imgur.com/rpHw2qGl.png)](http://i.imgur.com/rpHw2qG.png)



##### Creación de Geocercos
Los geocercos no son más que una superficie circular determinada por una coordenada que identifica el centro del círculo y un radio especificado en metros. Para crear un nuevo geocerco, clica sobre Coordenadas a la izquierda para seleccionar este tipo de filtro y luego sobre Añadir, en la parte superior derecha del mapa. En la pantalla te aparecerá un mapa como muestra la siguiente imagen. En esta pantalla deberás ubicar el marcador o POI en la posición deseada y elegir un radio con la barra deslizante de la parte inferior.  La región seleccionada aparecerá sombreada en el mapa. En la parte superior hay un cuadro de búsqueda, en él puedes escribir el nombre de la ciudad que te interesa, también podrás navegar sobre el mapa con el ratón. Una vez establecida la región de interés sobre el mapa, clica sobre Añadir abajo a la derecha.

[![](http://i.imgur.com/zCNidvZl.png)](http://i.imgur.com/zCNidvZ.png)



#### Filtros por Datos de Usuario
En esta sección podrás segmentar a tus usuarios de acuerdo a la información que tu aplicación haya obtenido del usuario. Para ello tu aplicación deberá registrar esta información en la plataforma Twinpush a través de los "Custom Properties", puedes ver cómo hacerlo en el manual descrito [aquí](http://www.twinpush.com/documentation/devices#post-set-custom-property). Por defecto Twinpush te ofrece información del idioma de tus usuarios.

[![](http://i.imgur.com/3aWGaLul.png)](http://i.imgur.com/3aWGaLu.png)


Con los "Custom Properties" podrás segmentar a tus usuarios por: edad, género, ocupación, intereses o cualquier otra información que quieras personalizar. Para añadir un filtro, clica sobre Otras propiedades, y luego sobre Añadir arriba a la derecha, te aparecerá una pantalla como la siguiente. En ella podrás añadir un filtro sobre cualquiera de las propiedades que encontrarás en un menu desplegable.

[![](http://i.imgur.com/0mCoZg2l.png)](http://i.imgur.com/0mCoZg2.png)


#### Filtros por Datos del Dispositivo
TwinPush pone a tu disposición la información obtenida del dispositivo con el cual el usuario accede a tu aplicación. Información como: Plataforma (iOS o Android), Fabricante, Modelo, Versión del sistema, Version de tu App, etc. Podrás segmentar a tus usuarios añadiendo un filtro sobre dicha información.

[![](http://i.imgur.com/Jl5GSg6l.png)](http://i.imgur.com/Jl5GSg6.png)




##### Estadísticas de uso de tu App
TwinPush también te permite segmentar a tus usuarios según el tiempo que usan tu App. Con esta funcionalidad podrás crear campañas que incentiven el uso de tu App, pudiendo premiar a tus usuarios con ofertas exclusivas para ese segmento. Para añadir un filtro de uso de tu App, clica sobre Estadísticas de uso, y luego sobre Añadir, arriba a la dechecha, a continuación verás una pantalla como la siguiente. Podrás añadir un filtro sobre: el tiempo total de uso, el periodo de inactividad, el tiempo medio de uso diario o el tiempo transcurrido desde la instalación.

[![](http://i.imgur.com/SPZbFJdl.png)](http://i.imgur.com/SPZbFJd.png)



#### Resumen
Una vez establecidos tus filtros, podrás añadir una condición a todos ellos en la sección Resumen, a la derecha de la pantalla. Recuerda que cada filtro creado se mostrará con un sub indice en su sección. Selecciona entre "Cualquiera" o "Todas" en el menú desplegable.

* **cualquiera**: tu segmento incluirá a los usuarios que cumplan cualquiera de los filtros, por ejemplo: si has creado dos filtros por ubicación, uno en Madrid y otro en Barcelona, al seleccionar *cualquiera*, tu segmento incluirá a los usuarios con dispositivos ubicados en Madrid o en Barcelona, es decir, en ambas ubicaciones. La opción *cualquiera*, abre más tu segmento.
* **todas**: tu segmento incluirá a los usuarios que cumplan todos los filtros a la vez, por ejemplo has creado un filtro por ubicación en Madrid y otro filtro por plataforma iOS, al seleccionar *todas*, tu segmento incluirá a todos los usuario de iOS y a la vez ubicados en Madrid. La opción *todas* restringe más tu segmento.

Finalmente, dale un nombre a tu segmento y guárdalo clicando Crear Segmento.

[![](http://i.imgur.com/1b4AEHal.png)](http://i.imgur.com/1b4AEHa.png)



## Audiencia / Usuarios

TwinPush muestra los datos estadísticos más relevantes de tus usuarios, agrupados en tres bloques de información, Datos de registros y uso de la Aplicación, datos de Usuarios y datos de Dispositivos.
Para conocer más en detalle los datos de nuestros usuarios y tener la posibilidad de exportarlos para su estudio exhaustivo, deberemos hacerlo en la vista de ["Dispositivos"]()

### Registros y Uso de la Aplicación:
**Nuevos registros**: Gráfica con los registros nuevos recibidos en los últimos 30 días.

**Plataforma**: Número de dispositivos por plataforma, iOS o Android.

**Datos de Uso**: Gráficas por tiempos de uso de la Aplicación.

* Ha usado la Aplicación más de ...
* Ha usado la Aplicación menos de ...
* Ha usado la Aplicación de media más de ...
* Ha instalado la Aplicación hace menos de ...
* No ha usado la aplicación desde hace ...


[![](http://i.imgur.com/ET4QuL5l.png)](http://i.imgur.com/ET4QuL5.png)



### Propiedades de Usuario:
Mediante la [Integración de la "custom properties" en el SDK](http://developers.twinpush.com/quickstart), TwinPush es capaz de recibir y mostrar los datos relativos a las propiedades de los usuarios, estas gráficas se configuran automáticamente según el tipo de propiedad.

[![](http://i.imgur.com/crNW27dl.png)](http://i.imgur.com/crNW27d.png)



### Datos de Dispositivo:
Los datos de dispositivo que se muestran gráficamente son:

* **Idioma** en el que está el dispositivo configurado.
* **Fabricante** del dispositivo.

[![](http://i.imgur.com/FQXfjaDl.png)](http://i.imgur.com/FQXfjaD.png)



## Dispositivos

En la vista de "Dispositivos", TwinPush permite la consulta de toda nuestra base de Usuarios y sus datos. 


La vista muestra los dispositivos con actividad más reciente registrada, así como un buscador por Alias para realizar una busqueda de un usuario en concreto. En tabla principal mostramos la información más relevante sobre el dispositivo:

* **Push**: Este campo mostrará un check cuando el usuario haya aprobado recibir notificaciones Push en su dispositivo.
* **Activo**: Este campo mostrará un check cuando el usuario sea activo (Haya registrado actividad al menos una vez en los últimos 6 meses).
* **Alias**: Muestra el alias asociado al dispositivo.
* **Plataforma**: Mostrará un icono de el sistema Operativo, iOS o Android, así como la versión del mismo.
* **Device ID**: Identificador interno del dispositivo en TwinPush.
* **Fecha de Registro**: Fecha de primer registro del dispositivo.
* **Fecha de última apertura**: Fecha de ultima apertura de la Aplicación.

[![](http://i.imgur.com/g9e7kXpl.png)](http://i.imgur.com/g9e7kXp.png)


Al hacer click sobre uno de los dispositivos se mostrará un modal con toda la información relativa a este dispositivo, además de la información general, se mostrará:

#### Datos de Dispositivo:
* Fabricante
* Modelo
* Código de dispositivo:
* Versión del Sistema Operativo
* Versión de la App
* Versión del SDK

#### Datos de Localización:
Siempre que se haya realizado una [integración del SDK](http://developers.twinpush.com/quickstart) con la geolocalización activa de dispositivos, TwinPush mostrará información relativa a la ubicación del mismo.

* País
* Estado
* Ciudad

#### Datos de Usuario (Custom Properties):
El [SDK](http://developers.twinpush.com/quickstart) permite el registro de propiedades de usuario en TwinPush que también se mostrarán en la vista de Dispositivos.

[![](http://i.imgur.com/7s0LY7il.png)](http://i.imgur.com/7s0LY7i.png)


### Exportación:
TwinPush permite la Descarga de toda nuestra base de usuarios mediante la Exportación de un fichero CSV que contiene toda la información de cada uno de los registros de dispositivos.

Al tratarse de una tarea "pesada" TwinPush ofrece la posibilidad de descargarse siempre el último fichero generado así como lanzar el proceso de nuevo para actualizar el fichero.

[![](http://i.imgur.com/wHu2r9pl.png)](http://i.imgur.com/wHu2r9p.png)



## Ajustes

En la pestaña de Ajustes tendremos acceso a los parámetros de configuración de nuestra Aplicación, las claves de autenticación para la integración o la gestión de equipo.

### Detalle de Aplicación:
Podremos modificar los datos principales de nuestra Aplicación, tales como:

* **Nombre**: Nombre de la Aplicación que se muestra en TwinPush.
* **Zona horaria**: Modificar la zona horaria por defecto de nuestra aplicación, esto influirá en el envío de notificaciones y la programación horaria de las campañas, aun así, en cada campaña podremos definir una zona horaria única para esta campaña.
* **Icono**: Subir imagen con el icono que identificará nuestra Aplicación en TwinPush.
* **Badge number**: Este parámetro configura el número que aparece encima del icono de nuestra aplicación en los dispositivos iOS. Siendo por defecto +1.

[![](http://i.imgur.com/8Suvoawl.png)](http://i.imgur.com/8Suvoaw.png)


### Claves de Autenticación:

En esta vista también podremos consultar nuestras claves de autenticación con el servidor, necesarias para la integración del [SDK de TwinPush](http://developers.twinpush.com/quickstart) en las aplicaciones.

* **Subdominio**: Mostrará el subdominio que debemos configurar en la integración del SDK, customizado en el caso de clientes Enterprise.
* **Application ID**: Identificador de Aplicación de TwinPush.
* **API Key**: Parámetro de autenticación para la Aplicación y su conexión con el servidor de TwinPush.
* **API Key Creator**: Parámetro de autenticación para la creación y envío de notificaciones desde otro servidor.


[![](http://i.imgur.com/Kus3weMl.png)](http://i.imgur.com/Kus3weM.png)


### Gestión de Equipo:
TwinPush permite la gestión de las personas que tendrán acceso a tu aplicación. La invitación se realiza introduciendo el email de la persona a la que deseemos dar acceso y TwinPush se encargará de enviar un un email al invitado indicándole los pasos a seguir para acceder a la aplicación.

Los usuarios invitados tendrán acceso completo a:

* Crear y enviar Notificaciones.
* Información de audiencia.

No tendrán acceso a:

* Ajustes de aplicación, incluyendo la consulta de API Keys y configuración de APNS o GCM.
* Gestión de equipo.

El administrador de la cuenta podrá y será el único capaz de revocar cualquier invitación.

[![](http://i.imgur.com/umwhPfzl.png)](http://i.imgur.com/umwhPfz.png)


### Configuración GCM y APNS:

En la vista de Ajustes de TwinPush también dispondremos del acceso a la configuración de los servicios de [APNS de Apple](https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html#//apple_ref/doc/uid/TP40008194-CH100-SW9/) y [GCM de Google](https://developers.google.com/cloud-messaging/), necesarios para el envío de Notificaciones Push a dispositivos. Estos datos deberán ser proporcionados por el equipo de desarrollo de la Aplicación, más información sobre esta configuración en la [documentación de integración del SDK](http://developers.twinpush.com/quickstart).

TwinPush mantiene un control sobre la fecha de caducidad de tu certificado de APNS, mostrando una alerta cuando reste poco tiempo para su caducidad, evitando errores de certificado no válido, que provocaría errores de envío a dispositivos iOS.


[![](http://i.imgur.com/kYLz8U0l.png)](http://i.imgur.com/kYLz8U0.png)


### Subir Certificado APNS:
En la subida del certificado de APNS que nos proporciona Apple, debemos tener en cuenta marcar nuestro certificado como "Desarrollo" si estamos compilando una versión de pruebas desde nuestro entorno o de "Producción" Si ya es una aplicación que va dirigida al Store.

[![](http://i.imgur.com/1mPVegKl.png)](http://i.imgur.com/1mPVegK.png)



## Preguntas Frecuentes

### Dispositivos / Usuarios:
**1. No se registran correctamente los dispositivos en la Plataforma**

**2. Los dispositivos nuevos registrados están inactivos**

En ocasiones los dispositivos registrados nuevos se marcan automáticamente como inactivos, esto podrás comprobarlo en la vista de "Dispositivos" del administrador.

Esto provoca que las notificaciones no se envíen a estos dispositivos. Muy probablemente se deba a un problema con los límites de tu licencia, informate sobre el funcionamiento de las [licencias de TwinPush aquí]().

**3. No se exporta el archivo de Dispositivos**







### Envío de Notificaciones:
**1. No recibo la notificación**

**Comprobaciones iniciales**

Lo primero que debemos hacer es descargar el PDF de reporte de la notificación que no ha sido entregada al dispositivo, en este informe, disponible en la vista de detalle de la Notificación, podremos ver si ha ocurrido algún error que TwinPush haya identificado, los errores más comunes son:

* No tiene envíos: No existían destinatarios ( dispositivos activos ) para nuestro envío, por lo que deberemos comprobar los destinatarios y que se encuentren activos, esto podremos consultarlo en la vista de "Dispositivos" de el Admin Web.
* Token No válido o error de certificado: Estos errores son habituales en envíos a iOS, requiere revisar la configuración de certificados de nuestra Aplicación, validez, caducidad, etc...

Si en el informe de TwinPush está registrado el envío correctamente sin errores, los motivos principales por los que no se recibe una notificación son los siguientes:

* Dispositivo Inactivo, principalmente ocasionado por un problema con el límite de la licencia.
* Notificaciones no habilitadas en el dispositivo, debemos comprobar que tenemos habilitadas las notificaciones y volver a realizar la prueba.
* Problemas de conectividad en el dispositivo, en ocasiones tanto la falta de cobertura como una conexión a una red corporativa pueden ocasionar que las notificaciones no lleguen o lo hagan con cierto retraso.
* La notificación si ha llegado pero ha pasado desapercibida.

En general, siempre que tengamos problemas para determinar por qué una notificación no llega a un dispositivo en concreto, debemos comprobar la actividad de nuestro dispositivo: En la vista de "Dispositivos" podremos encontrar nuestro dispositivo, asociado a nuestro usuario, debemos percatarnos que esté efectivamente activo y tenga habilitadas las notificaciones.

Podemos incluso reinstalar la Aplicación y repetir el proceso de registro.

Si has seguido todos estos pasos y aun así el problema persiste, ponte en contacto en [support@twincoders.com](mailto:support@twincoders.com)



**2. No aparece notificación en el buzón de la Aplicación**

TwinPush cuenta con un buzón por usuario, donde se podrán consultar las notificaciones de un usuario en concreto desde una Aplicación.

Si no visualizamos una campaña en el listado de notificaciones, lo más probable es que, en el momento del envío, no hayamos marcado la notificación para que se entregue en el buzón del usuario, requisito indispensable para que aparezca en el buzón posteriormente.


**3. El texto de la notificación no llega completo**

El tamaño de las notificaciones está limitado por Apple y Google.

En primer lugar debemos tener en cuenta que el tamaño viene delimitado por todo lo que compone nuestra notificación, como el texto de la Push, así como la url, custom properties, etc... Si toda esta petición sobrepasa el límite establecido, TwinPush recortará el texto del mensaje para garantizar la entrega del mensaje a todos los sistemas operativos.

Es por esto que en ocasiones, el texto de la notificación se verá recortado.

Para solucionarlo, podremos intentar recortar espacio de la URL o acortar el mensaje para que llegue completo.


**4. No se visualiza el contenido Web de la notificación**

La notificación llega pero el contenido web que debería visualizarse al abrir la notificación no abre en la Aplicación.

Si este es el problema deberemos hacer varias comprobaciones:

* Ver si la web destino carga correctamente en un navegador, en ocasiones el problema se debe a una sobrecarga en la Web destino provocada por las peticiones de los usuarios móviles en un momento dado.
* La Web tiene referencias a URL no seguras. En ciertos sistemas operativos, si la Web destino no es segura, no se abrirá dentro de nuestra aplicación, pudiendo obligarnos a abrirla en el navegador del dispositivo.

Si no visualizamos ningún contenido rico enviado, podríamos tener un problema de integración del Webview de la Aplicación y tendrás que ponerte en contacto con el equipo de desarrollo de tu Aplicación.


**5. Problemas al subir archivo de envío**

TwinPush ofrece la posibilidad de cargar ficheros con el target de nuestra campaña.

El procesamiento del archivo de TwinPush maneja ciertos formatos y composiciones y debes seguir la [guía de creación de Archivos de envío](https://twincoders.atlassian.net/wiki/display/TWP/Import+target+file) para su correcto funcionamiento.




### Otros
**1. No recibo emails de confirmación o invitación a una aplicación**

TwinPush te enviará un correo electrónico para confirmar tu cuenta, cambiar tu contraseña, darte acceso como invitado a una Aplicación u otras acciones que lo requieran, en ocasiones, el correo se almacena en la carpeta Spam o queda bloqueado por un sistema de seguridad de tu empresa, si no recibes el email, ponte en contacto con nosotros en [support@twincoders.com](mailto:support@twincoders.com)


**2. No tengo acceso a los ajustes de la Aplicación**

Unicamente el usuario administrador de la cuenta tiene acceso a los ajustes y configuración de la Aplicación, donde tendremos las claves de autenticación, gestión del equipo de usuarios, así como la configuración principal de la Aplicación.



### Funcionamiento de las licencias

TwinPush basa sus licencias en la contratación de un número de dispositivos activos.

Un dispositivo activo es aquel que se haya registrado en algún momento en la plataforma a través del acceso a la Aplicación y que haya accedido al menos una vez en un periodo de tiempo determinado. Por defecto, este periodo es de 6 meses, aunque este parámetro temporal es configurable por cuenta. 

Todo aquel dispositivo que lleve 6 meses sin registrar actividad pasará automáticamente a formar parte de los dispositivos inactivos, en cuyo caso dejará de recibir notificaciones, reportar estadísticas y contar como dispositivo dentro de licencia.

####Dispositivos fuera de licencia contratada
En el caso de que una cuenta sobrepase el número de dispositivos activos contratados, TwinPush marcará la cuenta como “fuera de licencia” y los dispositivos nuevos que se registren se marcarán como inactivos, esta comprobación se realiza semanalmente. 

De esta manera, los dispositivos activos serán aquellos que han mostrado actividad los últimos 6 meses y tengan una fecha de registro más antigua. 

####Configuración tiempo de actividad
Es posible configurar el periodo de tiempo sin actividad necesario para considerar un dispositivo como inactivo. Este periodo (en meses) se puede ajustar por cuenta. De este modo, es posible forzar que los dispositivos sin actividad se consideran inactivos antes en el tiempo. 

Este parámetro de configuración permitirá ajustar la cuenta para poder trabajar siempre con los más activos, pero se perderá la capacidad de comunicarse con los dispositivos menos activos, imposibilitando la realización de campañas para obtener recurrencia de estos usuarios. 
Si deseas cambiar este valor, deberás ponerte en contacto con[ support@twincoders.com](mailto:support@twincoders.com)